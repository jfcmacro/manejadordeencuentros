package co.edu.eafit.st0257.manejadordeencuentros.impls.jfcmacro;

import co.edu.eafit.st0257.manejadordeencuentros.Procesos;
import co.edu.eafit.st0257.manejadordeencuentros.ProcesoB;
import co.edu.eafit.st0257.manejadordeencuentros.IRoom;

public class ProcesoBImpls extends ProcesoB {

    public ProcesoBImpls(IRoom room, Integer id) {
	super(room, id);
    }

    public IRoom obtenerRoom() {
	return super.obtenerRoom();
    }

    public void limpiarListaEncuentros() {
	System.out.println("No hago nada: " + obtenerId());
    }

    public void adicionarEncuentro(Procesos procesos) {
	System.out.println("No hago nada: " + obtenerId());
    }

    public void run() {
	System.out.println("No hago nada: " + obtenerId());
    }
}