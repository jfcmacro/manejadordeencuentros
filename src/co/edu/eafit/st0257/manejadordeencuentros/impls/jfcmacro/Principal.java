package co.edu.eafit.st0257.manejadordeencuentros.impls.jfcmacro;

import co.edu.eafit.st0257.manejadordeencuentros.utils.AbsPrincipal;
import co.edu.eafit.st0257.manejadordeencuentros.utils.InternalException;

public class Principal extends AbsPrincipal {

    public Principal() throws InternalException {
	super();
    }

    public String obtenerNombrePaquete() {
	return "co.edu.eafit.st0257.manejadordeencuentros.impls.jfcmacro";
    }

    public static void main(String args[]) {
	try { 
	    AbsPrincipal principal = new Principal();
	    principal.ejecutar();
	} 
	catch (InternalException ie) {
	    System.err.println(ie);
	    System.exit(1);
	}
    }
}