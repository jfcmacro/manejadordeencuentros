package co.edu.eafit.st0257.manejadordeencuentros.utils;

public class InternalException extends Exception {
    private String message;

    public InternalException(String message, Throwable cause) {
	super(message, cause);
    }
}