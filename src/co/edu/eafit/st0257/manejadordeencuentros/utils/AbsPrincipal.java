package co.edu.eafit.st0257.manejadordeencuentros.utils;

import java.util.ArrayList;
import java.util.Random;
import java.lang.reflect.Constructor;
import co.edu.eafit.st0257.manejadordeencuentros.IRoom;
import co.edu.eafit.st0257.manejadordeencuentros.Procesos;
import co.edu.eafit.st0257.manejadordeencuentros.ProcesoA;
import co.edu.eafit.st0257.manejadordeencuentros.ProcesoB;

public abstract class AbsPrincipal {

    private Class classProcesoA;
    private Class classProcesoB;
    private Class classRoom;
    private IRoom room;
    private int min = 10;
    private int max = 50;
    private int id = 0;

    private ArrayList<Procesos> crearProcesosA()
	throws InternalException {

	Constructor<? extends ProcesoA> constructorA;
	try {
	    Class intClass = Integer.class;
	    constructorA = classProcesoA.getDeclaredConstructor(classRoom
								,intClass);
	}
	catch (Exception e) {
	    throw new
		InternalException("Obteniendo informacion de procesos A \n" 
				  + e,
				  e);
	}

	Random random = new Random(System.currentTimeMillis());
	int total = random.nextInt(max - min);

	ArrayList<Procesos> listaProcesosA = new ArrayList<Procesos>();
	for (int i = 0; i < total; i++) {
	    try {
		listaProcesosA.add(constructorA.newInstance(room,id++));
	    }
	    catch (Exception e) {
		throw new InternalException("Creando procesos A " + e,
					    e);
	    }
	}
	return listaProcesosA;
    }

    private ArrayList<Procesos> crearProcesosB()
	throws InternalException {

	Constructor<? extends ProcesoB> constructorB;

	try {
	    Class intClass = Integer.class;
	    constructorB  = classProcesoB.getDeclaredConstructor(classRoom
								 ,intClass);
	}
	catch(Exception e) {
	    throw new
		InternalException("Obteniendo informacion de procesos B " + e,
				  e);
	}

	ArrayList<Procesos> listaProcesosB = new ArrayList<Procesos>();
	Random random = new Random(System.currentTimeMillis());
	int total = random.nextInt(max - min);

	for (int i = 0; i < total; i++) {
	    try {
		listaProcesosB.add(constructorB.newInstance(room,id++));
	    }
	    catch (Exception e) {
		throw new InternalException("Creando procesos B " + e
					    , e);
	    }
	}
	return listaProcesosB;
    }

    protected void lanzarProcesos()
	throws InternalException {
	ArrayList<Procesos> procesos = crearProcesosA();
	if (!procesos.addAll(crearProcesosB())) {
	    throw new InternalException("Problema insertando en lista", null);
	}
	for (Procesos p : procesos) {
	    (new Thread(p)).start();
	}
    }

    protected abstract String obtenerNombrePaquete();

    protected AbsPrincipal() throws InternalException {
	try {
	    StringBuffer packageName = new StringBuffer();
	    packageName.append(obtenerNombrePaquete()).append(".");
	    StringBuffer nombreClase = new StringBuffer(packageName);
	    classProcesoA =
		Class.forName(nombreClase.append("ProcesoAImpls").toString());
	    nombreClase = new StringBuffer(packageName);
	    classProcesoB =
		Class.forName(nombreClase.append("ProcesoBImpls").toString());
	    nombreClase = new StringBuffer(packageName);
	    classRoom = IRoom.class; 
		// Class.forName(nombreClase.append("RoomImpls").toString());
	    Class classRoom2 = 
		Class.forName(nombreClase.append("RoomImpls").toString());
	    Constructor<? extends IRoom> roomConstructor =
		classRoom2.getDeclaredConstructor();
	    room = roomConstructor.newInstance();
	} catch (Exception e) {
	    throw new InternalException("Constructor " + e, e);
	}
    }

    public void ejecutar(int min, int max)
	throws InternalException {
	this.min = min;
	this.max = max;
	lanzarProcesos();
    }

    public void ejecutar()
	throws InternalException {
	lanzarProcesos();
    }
}