package co.edu.eafit.st0257.manejadordeencuentros;

public interface IRoom {
    public void enter(Procesos proceso);
}