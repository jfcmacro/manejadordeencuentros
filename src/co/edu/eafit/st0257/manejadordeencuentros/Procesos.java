package co.edu.eafit.st0257.manejadordeencuentros;


public abstract class Procesos implements Runnable {

    private IRoom room;
    private int id;

    public Procesos(IRoom room, Integer id) {
	this.room = room;
	this.id = id;
    }

    public void run() {
	while (true) {
	    room.enter(this);
	}
    }

    public IRoom obtenerRoom() {
	return room;
    }

    public abstract void limpiarListaEncuentros();

    public abstract void adicionarEncuentro(Procesos proceso);

    public int obtenerId() {
	return id;
    }
}